import Client from "../client/Client"
import axios from "axios";

const transformData = ({data, code}) => {
    return {
        isWrap: "0",
        wsCode: code,
        wsRequest: {
            token: localStorage.getItem("accessToken"),
            isdn: data.isdn ? data.isdn : localStorage.getItem("isdn"),
            wsCode: code,
            wsRequest: {
                ...data,
            }
        },
    }
}

const transformData2 = ({data, code}) => {
    return {
        isWrap: "1",
        wsCode: code,
        wsRequest: {
            ...data,
        },
    }
}

const searchByTitle = (data) => {
    return Client.post(``, transformData({data, code: "wsGetLatestNews"}))
}

const listByCategory = (data) => {
    return Client.post(``, transformData({data, code: "wsGetNewsByCategory"}))
}

const listByTag = (data) => {
    return Client.post(``, transformData({data, code: "wsGetNewsByTag"}))
}

const detail = (data) => {
    return Client.post(``, transformData({data, code: "wsGetNewsDetail"}))
}

const listCategories = (data) => {
    return Client.post(``, transformData({data, code: "wsGetCategories"}))
}

const listTag = (data) => {
    return Client.post(``, transformData({data, code: "wsGetListTags"}))
}

const search = (data) => {
    return Client.post(``, transformData({data, code: "wsSearch"}))
}

const login = (data) => {
    return Client.post(``, transformData2({data, code: "wsLoginOtpNewsService"}))
}

const otp = (data) => {
    return Client.post(``, ({
        isWrap: "1",
        wsRequest: data,
        wsCode: "wsGetOtpNewsService"
        // wsCode: "wsGetOtp",
    }))
}

const checkRegister = (data) => {
    return Client.post(``, ({
        isWrap: "1",
        wsRequest: data,
        wsCode: "wsCheckNewsService"
    }))
}

const refreshToken = (data) => {
    return axios.post(``, transformData2({data, code: "wsRefreshTokenNewsService"}))
}

const register = (data) => {
    return Client.post(``, transformData2({data, code: "wsRegisterNewsService"}))
}

const cancel = (data) => {
    return Client.post(``, transformData2({data, code: "wsCancelNewsService"}))
}

const commonRepository = {
    searchByTitle,
    listByCategory,
    listByTag,
    detail,
    listCategories,
    listTag,
    search,
    login,
    otp,
    refreshToken,
    register,
    cancel,
    checkRegister,
}
export default commonRepository
