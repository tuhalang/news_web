import getInstanceAxios from "../request"
import {BASE_URL} from "../../utils/constant";
const baseURL = `${BASE_URL}/`

export default getInstanceAxios(baseURL)
