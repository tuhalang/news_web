import React from "react";

import './index.scss'
import CardCategory from "../../components/Card/CardCategory";

function Category({}) {
    return (
        <div className="flex flex-column items-center">
            <CardCategory />
            <CardCategory />
            <CardCategory />
        </div>
    )
}

export default Category
