import React, {memo} from "react";
import {Tabs} from "antd";
import {useSelector} from "react-redux";
import all from "../../assets/images/category.png"

const {TabPane} = Tabs;

function TabHead({name, imageUrl}) {
    return (
        <div className="flex flex-column items-center">
            <img src={imageUrl} className="img-tab"/>
            <span className="text-tab">{name}</span>
        </div>
    )
}

const Filter = memo(({t, i18n, filter, onFilter}) => {

    const {categories} = useSelector(state => state.common)
    const {tags} = useSelector(state => state.common)
    const __filter = React.useRef({})
    const __timeOut = React.useRef()
    const sub = categories.find(item => item.id === filter.category)

    function onChangeFilter(key, value) {
        if (key === "category") {
            __filter.current.subCate = null
        }
        __filter.current[key] = value
        if (__timeOut.current) {
            clearTimeout(__timeOut.current)
        }
        __timeOut.current = setTimeout(() => {
            onFilter(__filter.current)
        }, 300)
    }

    const {subCate} = filter

    return (
        <div className="flex flex-column filter">
            <Tabs onChange={(key) => {
                onChangeFilter("category", key)
            }}>
                <TabPane tab={TabHead({ name: t('all'), imageUrl: all})} key={""}></TabPane>
                {categories.map(item => <TabPane tab={TabHead({...item})} key={item.id}>
                </TabPane>)}
            </Tabs>
            {sub && sub.subCategories && <div className="list-tag flex justify-start mb-3">
                {sub.subCategories.map(item => <div
                    className={`tag ${subCate === item.id ? 'tag-active' : ''}`}
                    key={item.id}
                    onClick={() => {
                        if (subCate === item.id) {
                            onChangeFilter("subCate", undefined)
                        } else
                            onChangeFilter("subCate", item.id)
                    }}>{item.name}</div>)}
            </div>}
        </div>
    )
})

export default Filter
