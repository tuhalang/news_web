import React from "react";
import {Input} from "antd";
import {useDispatch} from "react-redux";
import {
    listCategories,
    listPosts,
    listTags, postDetail, search,
} from "../../redux/actions";

import './index.scss'
import {SearchOutlined} from "@ant-design/icons";
import Pane from "./Pane";
import Filter from "./Filter";
import {withTranslation} from "react-i18next";
import {useParams} from "react-router";
import NewDetail from "../../components/NewDetail/NewDetail";
import {paramsUrl} from "../../components/_Function";


function Home({t, i18n}) {

    const dispatch = useDispatch()
    const [loading, setLoading] = React.useState(false)
    const { newsId } = useParams()
    const [detail, setDetail] = React.useState()
    const [__pagination, __setPagination] = React.useState({
        pageNumber: 1,
        pageSize: 10,
        total: 0,
        parentId: "",
        ...paramsUrl.get(),
    })

    function fetch() {
        setLoading(true)
        dispatch(search({
            ...__pagination,
            page: __pagination.pageNumber - 1,
            size: __pagination.pageSize,
            language: i18n.language,
            category: __pagination.subCate ? __pagination.subCate : __pagination.category,
            callback: () => {
                setLoading(false)
            }
        }))
    }

    function onFilter(params) {
        __setPagination({
            ...__pagination,
            ...params,
        })
    }

    function onSearch(e) {
        __setPagination({
            ...__pagination,
            title: e.target.value,
        })
    }

    React.useEffect(() => {
        dispatch(listCategories({
            page: 0,
            size: 50,
            parentId: "",
            language: i18n.language
        }))
        dispatch(listTags({page: 0, size: 20}))
    }, [])

    React.useEffect(() => {
        if (!newsId || newsId === "") {
            if ((!__pagination.title || __pagination.title === "") && (!__pagination.category || __pagination.category === "")) {
                setLoading(true)
                dispatch(listPosts({
                    ...__pagination,
                    language: i18n.language,
                    callback: () => setLoading(false)
                }))
            } else
                fetch()
        }
    }, [__pagination])

    React.useEffect(() => {
        if (newsId && newsId !== "") {
            setLoading(true)
            dispatch(postDetail({
                newsId, language: i18n.language, callbackSuccess: (data) => {
                    setDetail(data)
                },
                callback: () => setLoading(false)
            }))
        }
    }, [newsId])

    // React.useEffect(() => {
    //     console.log(params, "paramsUrl.get()")
    //     if (params.category && params.category !== "") {
    //         __setPagination({
    //             ...__pagination,
    //             ...params,
    //         })
    //     }
    // }, [params.category])


    return (
        <div className="flex flex-column p-1 w-full">
            {
                newsId && newsId !== "" && detail ? <NewDetail news={detail} loading={loading}/>
                    : <>
                        <Input className="w-full search mb-4" prefix={<SearchOutlined/>} placeholder={t('search')}
                               value={__pagination.title || ''}
                               onChange={onSearch}/>
                        {!(__pagination.title && __pagination.title != "") && <Filter
                            t={t} i18n={i18n}
                            filter={__pagination}
                            onFilter={onFilter}
                        />}
                        <Pane t={t} i18n={i18n} loading={loading} setDetail={setDetail} detail={detail}/>
                    </>
            }

        </div>
    )
}

export default withTranslation()(Home)
