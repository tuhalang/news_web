export const convertDate = (date) => {
    let tmp = new Date(date);
    return `${tmp.getDate()}/${tmp.getUTCMonth() + 1}/${tmp.getFullYear()}`
}
