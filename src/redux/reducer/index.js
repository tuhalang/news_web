import {combineReducers} from "redux";
import {
    LIST_CATEGORY_SUCCESS, LIST_POST_SUCCESS,
    LIST_TAG_SUCCESS, SEARCH_SUCCESS,
    SHOW_DRAWER,
    SHOW_OTP,
    SHOW_SUBSCRIBER,
    SHOW_VIDEO
} from "../actions/actions_type";

const defaultParams = {
    drawer: false,
    subscriber: false,
    video: undefined,
    otp: false,
    categories: [],
    tags: [],
    news: [],
    detail: null,
};

const common = (state = defaultParams, action) => {
    switch (action.type) {
        case SHOW_DRAWER:
            return {
                ...state,
                drawer: !state.drawer,
            }
        case SHOW_SUBSCRIBER:
            return {
                ...state,
                subscriber: action.visible,
            }
        case SHOW_VIDEO:
            return {
                ...state,
                video: action.path
            }
        case SHOW_OTP:
            return {
                ...state,
                otp: action.visible
            }
        case LIST_CATEGORY_SUCCESS:
            return {
                ...state,
                categories: action.payload.categories
            }
        case LIST_TAG_SUCCESS:
            return {
                ...state,
                tags: action.payload.tags,
            }
        case LIST_POST_SUCCESS:
            return {
                ...state,
                news: action.payload.news,
            }
        case SEARCH_SUCCESS:
            return {
                ...state,
                news: action.payload.news,
            }
        default:
            return {
                ...state,
            };
    }
}

const allReducers = combineReducers({
    common,
});

export default allReducers;
