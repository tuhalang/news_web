import _ from "lodash";
import {call, put, takeLatest} from "redux-saga/effects";
import {message} from "antd";
import getFactory from "../../api"
import {listCategoriesSuccess, listPostSuccess, listTagsSuccess, postDetailSuccess, searchSuccess} from "../actions";
import {
    CANCEL_SERVICE,
    GET_OTP,
    LIST_CATEGORY,
    LIST_POST,
    LIST_POST_BY_CATEGORY,
    LIST_POST_BY_TAG,
    LIST_TAG, LOGIN,
    POST_DETAIL, REFRESH_TOKEN, REGISTER_SERVICE, SEARCH, CHECK_REGISTER
} from "../actions/actions_type";

const api = getFactory("common")

function* searchByTitle(action) {
    console.log("======== searchByTitle ========", action)
    try {
        const res = yield call((payload) => api.searchByTitle(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                yield put(listPostSuccess({payload: result.wsResponse}))
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* listPostByCategory(action) {
    console.log("======== listPostByCategory ========", action)
    try {
        const res = yield call((payload) => api.listByCategory(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
            }
            // yield put(listPostSuccess({payload: data}))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* listPostByTag(action) {
    console.log("======== listPostByTag ========", action)
    try {
        const res = yield call((payload) => api.listByTag(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
            }
            // yield put(listPostSuccess({payload: data}))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* postDetail(action) {
    console.log("======== postDetail ========", action)
    try {
        const res = yield call((payload) => api.detail(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                yield put(postDetailSuccess(result.wsResponse))
                if (_.isFunction(action.callbackSuccess)) action.callbackSuccess(result.wsResponse.news)
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* listCategories(action) {
    console.log("======== listCategories ========", action)
    try {
        const res = yield call((payload) => api.listCategories(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                console.log(result)
                yield put(listCategoriesSuccess({data: result.wsResponse}))
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* listTags(action) {
    console.log("======== listTags ========", action)
    try {
        const res = yield call((payload) => api.listTag(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                yield put(listTagsSuccess(result.wsResponse))
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* search(action) {
    console.log("======== search ========", action)
    try {
        const res = yield call((payload) => api.search(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                yield put(searchSuccess(result.wsResponse))
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* login(action) {
    console.log("======== login ========", action)
    try {
        const res = yield call((payload) => api.login(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            // yield put(searchSuccess(data))
            const {errorCode} = result
            if (errorCode == "0") {
                const {isRegister, refreshToken, token} = result.wsResponse
                localStorage.setItem("isRegister", isRegister)
                localStorage.setItem("refreshToken", refreshToken)
                localStorage.setItem("accessToken", token)
                localStorage.setItem("isdn", action.payload.isdn)
                if (_.isFunction(action.callbackSuccess)) action.callbackSuccess(isRegister)
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* otp(action) {
    console.log("======== otp ========", action)
    try {
        const res = yield call((payload) => api.otp(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            // yield put(searchSuccess(data))
            const {errorCode} = result
            if (errorCode == "0") {
                if (_.isFunction(action.callbackSuccess)) action.callbackSuccess()
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* refresh(action) {
    console.log("======== refresh ========", action)
    try {
        const res = yield call((payload) => api.refreshToken(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
            }
            // yield put(searchSuccess(data))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* register(action) {
    console.log("======== register ========", action)
    try {
        const res = yield call((payload) => api.register(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                if (_.isFunction(action.callbackSuccess)) action.callbackSuccess()
            } else {
                message.error(result.message)
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function* cancel(action) {
    console.log("======== cancel ========", action)
    try {
        const res = yield call((payload) => api.cancel(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                // message.success(res.message)
                if (_.isFunction(action.callbackSuccess)) action.callbackSuccess()
            } else {
                message.error(res.message)
            }
            // yield put(searchSuccess(data))
        }
    } catch (e) {
        console.log(e)
    } finally {
        if (_.isFunction(action.callback)) action.callback()
    }
}

function *checkRegister(action) {
    console.log("======== checkRegister ========", action)
    try {
        const res = yield call((payload) => api.checkRegister(payload), action.payload)
        const {
            errorCode,
            result,
            errorMessage,
        } = res
        if (errorCode != "S200") {
            message.error(errorMessage)
        } else {
            const {errorCode} = result
            if (errorCode == "0") {
                localStorage.setItem("isRegister", true)
                if (_.isFunction(action.callback)) action.callback(true);
            } else if (errorCode == "2") {
                message.error("Você não se registrou para este serviço !")
                setTimeout(() => {
                    localStorage.clear();
                    window.location.href = "/login";
                }, 1500)
            }
        }
    } catch (e) {
        console.log(e)
    }
}

function* rootSaga() {
    yield takeLatest(LIST_POST, searchByTitle);
    yield takeLatest(LIST_POST_BY_CATEGORY, listPostByCategory);
    yield takeLatest(LIST_POST_BY_TAG, listPostByTag);
    yield takeLatest(POST_DETAIL, postDetail);
    yield takeLatest(LIST_CATEGORY, listCategories);
    yield takeLatest(LIST_TAG, listTags);
    yield takeLatest(SEARCH, search);
    yield takeLatest(LOGIN, login);
    yield takeLatest(GET_OTP, otp);
    yield takeLatest(REFRESH_TOKEN, refresh);
    yield takeLatest(REGISTER_SERVICE, register);
    yield takeLatest(CANCEL_SERVICE, cancel);
    yield takeLatest(CHECK_REGISTER, checkRegister);
}

export default rootSaga;
