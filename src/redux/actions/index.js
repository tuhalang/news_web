import {
    CANCEL_SERVICE,
    CHECK_REGISTER,
    GET_OTP,
    LIST_CATEGORY,
    LIST_CATEGORY_SUCCESS,
    LIST_POST,
    LIST_POST_BY_CATEGORY,
    LIST_POST_BY_TAG,
    LIST_POST_SUCCESS,
    LIST_TAG,
    LIST_TAG_SUCCESS, LOGIN,
    POST_DETAIL,
    POST_DETAIL_SUCCESS, REFRESH_TOKEN, REGISTER_SERVICE,
    SEARCH,
    SEARCH_SUCCESS,
    SHOW_DRAWER,
    SHOW_OTP,
    SHOW_SUBSCRIBER,
    SHOW_VIDEO
} from "./actions_type";
import {GAME_CODE} from "../../utils/constant";

export const showDrawer = () => {
    return {
        type: SHOW_DRAWER
    }
}

export const showSubscriber = (visible) => {
    return {
        type: SHOW_SUBSCRIBER,
        visible,
    }
}

export const showVideo = (path) => {
    return {
        type: SHOW_VIDEO,
        path,
    }
}

export const showOtp = (visible) => {
    return {
        type: SHOW_OTP,
        visible,
    }
}

export const listPosts = ({page, size, title, language, callback}) => {
    return {
        type: LIST_POST,
        payload: {
            page,
            size,
            title,
            language,
        },
        callback,
    }
}

export const listPostSuccess = ({payload}) => {
    return {
        type: LIST_POST_SUCCESS,
        payload,
    }
}

export const listPostByCategory = ({page, size, category, language}) => {
    return {
        type: LIST_POST_BY_CATEGORY,
        payload: {
            page,
            size,
            category,
            language,
        }
    }
}

export const listByTag = ({page, size, tag, language}) => {
    return {
        type: LIST_POST_BY_TAG,
        payload: {
            page,
            size,
            tag,
            language,
        }

    }
}

export const postDetail = ({newsId, shortLink, language, callback, callbackSuccess}) => {
    return {
        type: POST_DETAIL,
        payload: {
            newsId,
            shortLink,
            language,
        },
        callback,
        callbackSuccess,
    }
}

export const postDetailSuccess = (data) => {
    return {
        type: POST_DETAIL_SUCCESS,
        payload: { ...data }
    }
}

export const listCategories = ({page, size, parentId, language}) => {
    return {
        type: LIST_CATEGORY,
        payload: {
            page,
            size,
            parentId,
            language,
        }
    }
}

export const listCategoriesSuccess = ({data}) => {
    return {
        type: LIST_CATEGORY_SUCCESS,
        payload: {...data}
    }
}

export const listTags = ({page, size}) => {
    return {
        type: LIST_TAG,
        payload: {
            page,
            size,
        }
    }
}

export const listTagsSuccess = (data) => {
    return {
        type: LIST_TAG_SUCCESS,
        payload: {...data}
    }
}

export const search = ({page, size, title, category, tag, language, callback}) => {
    return {
        type: SEARCH,
        payload: {
            page,
            size,
            title,
            category,
            tag,
            language,
        },
        callback,
    }
}

export const searchSuccess = (data) => {
    return {
        type: SEARCH_SUCCESS,
        payload: {...data}
    }
}

export const login = ({isdn, otp, gameCode = GAME_CODE, language, callback, callbackSuccess}) => {
    return {
        type: LOGIN,
        payload: {
            isdn,
            otp,
            gameCode,
            language,
        },
        callback,
        callbackSuccess,
    }
}

export const otp = ({isdn, gameCode = GAME_CODE, language, callback, callbackSuccess}) => {
    return {
        type: GET_OTP,
        payload: {
            isdn,
            gameCode,
            language,
        },
        callbackSuccess,
        callback,
    }
}

export const refreshToken = ({isdn, refreshToken, gameCode = GAME_CODE, language, callback}) => {
    return {
        type: REFRESH_TOKEN,
        payload: {
            isdn,
            refreshToken,
            gameCode,
            language,
        },
        callback,
    }
}

export const register = ({isdn, gameCode = GAME_CODE, otpType, otp, transid, language, callback, callbackSuccess}) => {
    return {
        type: REGISTER_SERVICE,
        payload: {
            isdn,
            gameCode,
            otpType,
            transId: transid,
            otp,
            language,
        },
        callback,
        callbackSuccess,
    }
}

export const cancelService = ({isdn, gameCode = GAME_CODE, otpType, transid, otp, language, callback, callbackSuccess}) => {
    return {
        type: CANCEL_SERVICE,
        payload: {
            isdn,
            gameCode,
            otpType,
            transId: transid,
            language,
            otp,
        },
        callback,
        callbackSuccess,
    }
}

export const checkRegister = (isdn, callback) => {
    return {
        type: CHECK_REGISTER,
        payload: {
            isdn,
            gameCode: GAME_CODE
        },
        callback
    }
}
