import React from 'react';
import ReactDOM from 'react-dom';

import './assets/css/index.css';
import 'antd/dist/antd.css';
import 'video-react/dist/video-react.css';
import './locales/i18n'
import './utils/env'

import reportWebVitals from './reportWebVitals';
import {BASE_URL} from "./utils/constant";
import createSagaMiddleware from 'redux-saga';
import {applyMiddleware, compose, createStore} from "redux";
import axios from "axios";
import {Provider} from "react-redux";
import allReducers from "./redux/reducer";
import rootSaga from "./redux/saga";
import Routes from "./routes";

window.axios = axios.create({
    baseURL: BASE_URL,
});

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    : compose;

export const store = createStore(
    allReducers,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
);
sagaMiddleware.run(rootSaga);

window.$dispatch = store.dispatch;
store.dispatch({
    type: '@@__INIT__',
});

document.title = 'Sport news'

ReactDOM.render(
    <Provider store={store}>
        <React.StrictMode>
            <Routes/>
        </React.StrictMode>
    </Provider>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
