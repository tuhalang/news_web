import React, {Suspense, useEffect, Fragment} from "react"
import {
    Route,
    Switch,
    Redirect,
    Link, HashRouter,
} from "react-router-dom"

import {Result} from "antd"
import Layout from "./components/layout/Layout";
import {useDispatch} from "react-redux";
import SuspenseComponent from "./components/router/SuspenseComponent";
import Login from "./components/layout/Login";

const Home = React.lazy(() => import("./pages/Home/Home"))

function PrivateRoute({children, ...rest}) {
    const checkToken = localStorage.getItem("accessToken")
    const checkRegister = localStorage.getItem("isRegister")
    // const checkToken = true
    return (
        <Route
            {...rest}
            render={() => (checkToken && checkRegister ? children : <Redirect to="/login"/>)}
        />
    )
}

const ProtectedPage = () => {
    const dispatch = useDispatch()

    function WaitingComponent(Component, code) {
        return (props) => (
            <Suspense fallback={<SuspenseComponent/>}>
                <Component {...props} />
            </Suspense>
        )
    }

    useEffect(() => {
    }, [])

    return (
        <Fragment>
            <Layout>
                <Switch>
                    {/*<Route*/}
                    {/*    path="/shipping-orders/upload"*/}
                    {/*    exact*/}
                    {/*    component={WaitingComponent(*/}
                    {/*        OrderUploadShipping,*/}
                    {/*        "SHIPPING_ORDER"*/}
                    {/*    )}*/}
                    {/*/>*/}
                    <Route path="/:newsId" exact component={WaitingComponent(Home, "HOME")}/>
                    <Route path="/" exact component={WaitingComponent(Home, "HOME")}/>
                    <Route component={NotLoadAuthorization}/>
                </Switch>
            </Layout>
        </Fragment>
    )
}

const Routes = () => {
    return (
        <HashRouter>
            <Switch>
                <Route exact path="/login" component={Login}/>
                <PrivateRoute path="/">
                    <ProtectedPage/>
                </PrivateRoute>
            </Switch>
        </HashRouter>
    )
}

export default Routes

const NotLoadAuthorization = () => {
    return (
        <Result
            status="404"
            title="404"
            subTitle="Trang không tồn tại"
            extra={<Link to="/">Trở về trang chủ</Link>}
        />
    )
}
