import React from "react";

import './index.scss'
import {useDispatch, useSelector} from "react-redux";
import {otp, showSubscriber, register} from "../../redux/actions";
import {CloseOutlined, PhoneOutlined} from "@ant-design/icons";
import {Alert, Button, Checkbox, Input, message, Tag} from "antd";
import OTPModal from "./Otp";
import {useHistory} from "react-router-dom";
import {withTranslation} from "react-i18next";

const uuidv4 = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function useOutsideAlerter(ref, onAction) {
    React.useEffect(() => {
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
                onAction()
            }
        }

        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [ref]);
}

function Subscriber({visible, t}) {

    const dispatch = useDispatch()
    const ref = React.useRef()
    const history = useHistory()
    const contentRef = React.useRef(null);
    const [uuid, setUuid] = React.useState('')
    // useOutsideAlerter(contentRef, () => {
    //     dispatch(showSubscriber(false))
    // })

    const [otpVisible, setOtpVisible] = React.useState(false)

    function sendOtp() {
        const _uuid = uuidv4()
        setUuid(_uuid)
        dispatch(register({
            isdn: localStorage.getItem("isdn"),
            otpType: "0",
            transid: _uuid,
            callbackSuccess: () => {
                setOtpVisible(true)
            }
        }))
    }

    function subscriber({otp}) {
        dispatch(register({
            isdn: localStorage.getItem("isdn"),
            otpType: "1",
            otp,
            transid: uuid,
            callbackSuccess: () => {
                setOtpVisible(false)
                localStorage.setItem("isRegister", "true")
                dispatch(showSubscriber(false))
                message.success("Subscribe success")
            }
        }))
    }

    React.useEffect(() => {
        if (!visible && contentRef) {
            contentRef.current.classList.add("content--close")
            ref.current.classList.add("subscriber--close")
        } else {
            contentRef.current.classList.remove("content--close")
            ref.current.classList.remove("subscriber--close")
        }
    }, [visible])

    return (
        <div className="subscriber absolute" ref={ref}>
            <div className="content-subscriber flex flex-column" ref={contentRef}>
                <div className="flex justify-between w-full p-3">
                    <div className="w-8"></div>
                    <span className="title-header">Sport news</span>
                    <CloseOutlined className="w-8 click" onClick={() => dispatch(showSubscriber(false))}/>
                </div>
                <div className="p-4 flex flex-column flex-full">
                    <Input placeholder="Phone number" prefix={<PhoneOutlined/>} className="w-full phone"
                           value={localStorage.getItem("isdn")} disabled/>
                    <Alert message={"2 MT/dia e renovação automática"} type="warning" className="w-full" style={{
                        margin: "0.5em 0 0.5em 0",
                    }} banner/>
                    {/* <Checkbox
                        checked
                        disabled
                        className="checkbox"
                    >
                        {'Nhập mã OTP được gửi tới số điện thoại của bạn'}
                    </Checkbox> */}
                    {/* <span
                        className="p-2 text-info">{'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tristique eros consequat dolor maximus.'}</span> */}
                    <div className="flex-full flex flex-column justify-center">
                        <Button className="w-40 btn mb-5" onClick={sendOtp}>Subscriber</Button>
                    </div>
                </div>
            </div>
            <OTPModal
                visible={otpVisible}
                reSend={() => {
                    sendOtp()
                }}
                handleOk={subscriber}
                title={t('otp-modal-title')}
                onClose={() => setOtpVisible(false)}
            />
        </div>
    )
}

export default withTranslation("translation")(Subscriber)
