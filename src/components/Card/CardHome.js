import React from "react";

import './index.scss'
import play from '../../assets/images/play-img.png'
import playMini from '../../assets/images/play-mini.png'
import img from '../../assets/images/main-img.png'
import {convertDate} from "../../utils";
import {useHistory} from "react-router";

const convertTitle = (title) => {
    return title.length > 38 ? title.substring(0, 38) + '...' : title
}

function CardItem({news}) {

    const history = useHistory()

    return (
        <div className="flex flex-column w-30" style={{
            height: 110
        }}>
            {news && news.imageUrl && <div className="card-item" onClick={() => history.push(`/${news.id}`)}>
                <img className="main-img" src={news.imageUrl} style={{
                    height: 60
                }}/>
                <div className="flex items-center justify-center w-full play-img h-100">
                    {/* <img src={playMini}/> */}
                </div>
            </div>}
            <span className="desc-item" onClick={() => history.push(`/${news.id}`)}>{convertTitle(news.title)}</span>
        </div>
    )
}

function CardHome({onDetail, news}) {

    const history = useHistory()

    const compareJustNow = (time) => {
        let mm = new Date().getTime() - new Date(time)
        if (mm > 0 && mm < 7200000) return "Just now"
        else return ""
    }


    return (
        <div className="card">
            {news && news.imageUrl && <div className="thumbnail-video">
                <img className="main-img" src={news.imageUrl} style={{
                    height: 200
                }}/>
                {news.videoUrl && 
                    <div className="flex items-center justify-center w-full play-img h-100"
                        onClick={() => history.push(`/${news.id}`)}>
                        <img src={play}/>
                    </div>
                }
                
            </div>}
            <div className="w-full flex justify-between mt-2 mb-2">
                <span className="name">{news.category || ''}</span>
                <span className="time">{`${compareJustNow(news.createdAt)} ${convertDate(news.createdAt)}`}</span>
            </div>
            <span className="desc" onClick={() => history.push(`/${news.id}`)}>{news.title}</span>
            <div className="w-full flex justify-between items-center mb-4 mt-3">
                {news && news.similarNews && news.similarNews.map((item) => <CardItem news={item}/>)}
            </div>
        </div>
    )
}

export default CardHome
