import React from "react";

import './index.scss'
import play from '../../assets/images/play-img.png'
import playMini from '../../assets/images/play-mini.png'
import img from '../../assets/images/main-img.png'

function CardItem({}) {
    return (
        <div className="flex flex-column w-30">
            <div className="card-item">
                <img className="main-img" src={img}/>
                <div className="flex items-center justify-center w-full play-img h-100">
                    <img src={playMini}/>
                </div>
            </div>
            <span className="desc-item">{'Italy won’t face oke anything like ...'}</span>
        </div>
    )
}

function CardCategory({onDetail}) {
    return (
        <div className="card">
            <span className="name mt-1 mb-2">Football</span>
            <div className="thumbnail-video">
                <img className="main-img" src={img}/>
                <div className="flex items-center justify-center w-full play-img h-100" onClick={() => onDetail(1)}>
                    <img src={play}/>
                </div>
            </div>
            <span className="time mt-2 mb-2">{'Vừa xong 08/07/2021'}</span>
            <span className="desc">{'Italy won’t face anything like Spai in the Euro 2020 \n' +
            'final. Italy won’t face anything like Spai in the ...'}</span>
            <div className="w-full flex justify-between items-center mb-4 mt-3">
                <CardItem/>
                <CardItem/>
                <CardItem/>
            </div>
        </div>
    )
}

export default CardCategory
