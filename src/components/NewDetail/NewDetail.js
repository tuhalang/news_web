import React from "react";
import img from "../../assets/images/main-img.png";
import play from "../../assets/images/play-img.png";
import './index.scss'
import {convertDate} from "../../utils";
import {useDispatch} from "react-redux";
import {showVideo, checkRegister, showSubscriber} from "../../redux/actions";
import {Skeleton, Tag} from "antd";
import {renderColor} from "../_Function";
import {useHistory} from "react-router-dom";
import Subscriber from "../modal/Subscriber";

const compareJustNow = (time) => {
    let mm = new Date().getTime() - new Date(time)
    if (mm > 0 && mm < 7200000) return "Just now"
    else return ""
}

function NewDetail({news, loading}) {

    const dispatch = useDispatch()
    const history = useHistory()
    // const {subscriber} = React.useSelector(state => state.common)

    React.useEffect(() => {
        dispatch(checkRegister(
            localStorage.getItem("isdn"),
            (isRegister) => {
                console.log("passs")
            }
        ))
    })

    return (
        <Skeleton loading={loading} className="w-full flex flex-column ">
            <span
                className="title">{news.title}</span>
            <div className="mt-2 mb-2">
                {news.tags.split(",").map((item, index) => <Tag color={renderColor(index)} onClick={()=>history.push("/")}>{item}</Tag>)}
            </div>
            <span className="mt-1 mb-2" style={{
                textAlign: "justify"
            }}>{news.summary}</span>
            <span className="time mt-2 mb-2">{`${compareJustNow(news.createdAt)} ${convertDate(news.createdAt)}`}</span>
            {news && news.imageUrl && <div className="thumbnail-video w-full">
                <img className="main-img" src={news.imageUrl} style={{
                    minHeight: 165
                }}/>
                {news.videoUrl &&
                    <div className="flex items-center justify-center w-full play-img h-100"
                        onClick={() => dispatch(showVideo(news.videoUrl))}>
                        <img src={play}/>
                    </div>
                }
            </div>}
            {/* <span className="caption-img mt-1 mb-2">{`Source: ${news.createdBy}`}</span> */}
            {/*<span className='desc'>{''}</span>*/}
            <div dangerouslySetInnerHTML={{__html: news.content}} className="content-detail" style={{
                textAlign: "justify"
            }}/>
            {/* <Subscriber visible={subscriber}/> */}
        </Skeleton>
    )
}

export default NewDetail
