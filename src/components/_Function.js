import queryString from "query-string"

export const paramsUrl = {
    get: () => {
        return queryString.parse(window.location.search)
    },
    set: (params) => {
        const currentUrlParams = queryString.stringify(params, {
            skipNull: true,
        })
        window.history.pushState(
            {},
            null,
            `${window.location.pathname}?${currentUrlParams.toString()}`
        )
    },
}

export function renderColor(value) {
    const data = Number(value)
    switch (data) {
        case 1:
            return "magenta"
        case 2:
            return "red"
        case 3:
            return "volcano"
        case 4:
            return "orange"
        case 5:
            return "gold"
        case 6:
            return "lime"
        case 7:
            return "green"
        case 8:
            return "cyan"
        default:
            return "blue"
            break
    }
}
