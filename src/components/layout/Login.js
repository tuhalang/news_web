import React from "react";

import './index.scss'
import {isMobile} from "react-device-detect";
import Header from "./header/Header";

import img from "../../assets/images/login-bg.png"
import {Button, Form, Input, Skeleton} from "antd";
import {PhoneFilled} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import {login, otp, showSubscriber} from "../../redux/actions";
import {withTranslation} from "react-i18next";
import OTPModal from "../modal/Otp";
import Lottie from 'react-lottie';
import animationData from "../../assets/global.json";
import {useHistory} from "react-router-dom";
import Subscriber from "../modal/Subscriber";

function Login({i18n, t}) {

    const dispatch = useDispatch()
    const history = useHistory()
    const {subscriber} = useSelector(state => state.common)
    const [loading, setLoading] = React.useState(true)
    const [visible, setVisible] = React.useState(false)
    const [isdn, setIsdn] = React.useState()

    const onFinish = ({isdn}) => {
        setIsdn(isdn)
        dispatch(otp({
            isdn,
            language: i18n.language,
            callbackSuccess: () => {
                setVisible(true)
            },
            callback: () => {
            }
        }))
    }

    const onLogin = ({otp}) => {
        setLoading(true)
        dispatch(login({
            isdn,
            otp,
            callback: () => {
                setLoading(false)
            },
            callbackSuccess: (isRegister) => {
                if (isRegister === true)
                    history.push("/")
                else {
                    setVisible(false)
                    dispatch(showSubscriber(true))
                }
            }
        }))
    }

    return (
        <div className={`layout flex flex-column absolute items-center`} style={isMobile ? {
            width: '100%'
        } : {}}>
            <div className="header justify-between items-center">
                <Header/>
            </div>
            <div className="w-full flex-grow flex flex-column">
                {/*<img src={img} className="login-img"/>*/}
                <div className="w-full mt-4">
                    <Lottie options={defaultOptions}
                            height={250}
                            width={250}/>
                </div>
                <div className="login-form flex flex-column items-center flex-grow" style={{
                    backgroundColor: "white",
                }}>
                    <span className="login-label" style={{
                        marginBottom: isMobile ? 18 : 45
                    }}>{ t('login')}</span>
                    <Form
                        onFinish={onFinish}
                        id={"login"}
                        footer={[]}
                    >
                        <Form.Item
                            name={"isdn"}
                            rules={[
                                {required: true, message: "Please input your phone"},
                            ]}
                        >
                            <Input prefix={<PhoneFilled/>} type={'tel'} placeholder={t('phone')}/>
                        </Form.Item>
                        {/*<Form.Item*/}
                        {/*name={""}*/}
                        {/*>*/}
                        {/*    <Input prefix={<LockFilled/>} placeholder={'Password'} type={'password'}*/}
                        {/*           suffix={<EyeFilled style={{*/}
                        {/*               color: hidden ? "#C4C4C4" : PRIMARY_COLOR*/}
                        {/*           }} onClick={() => setHidden(!hidden)}/>}/>*/}
                        {/*</Form.Item>*/}
                    </Form>
                    {/*<a>Resend OTP</a>*/}
                    <div className="w-full items-center">
                        <Button type={'primary'} className="w-full btn-login" form={"login"}
                                loading={visible}
                                htmlType={"submit"}>{t('login')}</Button>
                    </div>
                    {/* <span className="w-full desc-login">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tristique eros consequat </span> */}
                </div>
            </div>
            <OTPModal
                visible={visible}
                title={t('otp-modal-title')}
                handleOk={onLogin}
                reSend={() => {
                    onFinish({isdn})
                }}
                onClose={() => setVisible(false)}
            />
            <Subscriber visible={subscriber}/>
        </div>
    )
}

export default withTranslation("translation")(Login)

const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
    }
};
