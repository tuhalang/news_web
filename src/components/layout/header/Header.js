import React from "react";
import './index.scss'
import uk from "../../../assets/images/GB.png";
import mozambique from "../../../assets/images/mozambique.png";
import {withTranslation} from "react-i18next";
import {PRIMARY_COLOR} from "../../../utils/constant";
import {MenuOutlined} from "@ant-design/icons";
import {useDispatch} from "react-redux";
import {cancelService, register, showDrawer, showSubscriber} from "../../../redux/actions";
import user from "../../../assets/images/user.png"
import user_menu from '../../../assets/user_menus.json'
import Dropdown from "../../dropdown/Dropdown";
import {Link, useHistory} from "react-router-dom";
import OTPModal from "../../modal/Otp";
import {message} from "antd";

const listLanguage = [
    {
        name: 'en',
        image: uk
    },
    {
        name: 'pt',
        image: mozambique
    },
];

const uuidv4 = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function Header({t, i18n, icon, onBackPage}) {

    const dispatch = useDispatch()
    const history = useHistory()
    const [otpVisible, setOtpVisible] = React.useState(false)
    const [uuid, setUuid] = React.useState('')
    const isLogin = () => localStorage.getItem('accessToken') && localStorage.getItem('accessToken') != '';


    function onAction(id) {
        if (id === "logout") {
            localStorage.clear()
            history.replace("/")
        } else if (id === "cancel") {
            sendOtp()
        }
    }

    function sendOtp() {
        console.log("sendOtp")
        const _uuid = uuidv4()
        setUuid(_uuid)
        dispatch(cancelService({
            isdn: localStorage.getItem("isdn"),
            otpType: "0",
            transid: _uuid,
            callbackSuccess: () => {
                setOtpVisible(true)
            }
        }))
    }

    function cancel({otp}) {
        dispatch(cancelService({
            isdn: localStorage.getItem("isdn"),
            otpType: "1",
            otp,
            transid: uuid,
            callbackSuccess: () => {
                message.success("Cancel service success")
                setOtpVisible(false)
                localStorage.clear()
                history.replace("/")
            }
        }))
    }

    return (
        <div className="header">
            <div className="container">
                <MenuOutlined className="icon flex start click" onClick={() => dispatch(showDrawer())}/>
                {/*<div className="icon" onClick={onBackPage}>{*/}
                {/*    icon ? <FontAwesomeIcon icon={faChevronLeft} size={11}/> : <div/>*/}
                {/*}</div>*/}
                <span className="title" onClick={() => history.push("/")} style={{
                    cursor: "pointer",
                }}>{ t('title') }</span>
                <div className="icon justify-end flex items-center">
                    {/*<img src={listLanguage.filter(item => item.name === i18n.language)[0].image}*/}
                    {/*     className="language click"*/}
                    {/*     onClick={() => i18n.changeLanguage(i18n.language === 'en' ? 'pt' : 'en')}/>*/}
                    {listLanguage.map(e => <img src={e.image} className="flags" style={{
                        backgroundColor: i18n.language == e.name ? 'white' : PRIMARY_COLOR,
                    }} onClick={() => i18n.changeLanguage(e.name)}/>)}
                    {isLogin() && <Dropdown
                        customToggle={() => <img className="user click" src={user}/>}
                        contentData={user_menu}
                        renderItems={(item, index) => renderUserMenu(item, index, onAction)}
                    />}

                </div>
            </div>
            <div style={{
                height: 3,
                border: '0.5px solid white',
                backgroundColor: PRIMARY_COLOR,
            }}></div>
            <OTPModal
                visible={otpVisible}
                reSend={() => {
                    sendOtp()
                }}
                handleOk={cancel}
                title={t('otp-modal-title')}
                onClose={() => setOtpVisible(false)}
            />
        </div>
    )
}

export default withTranslation("translation")(Header);

const renderUserMenu = (item, index, onAction) => (
    <Link to='/' key={index} onClick={() => onAction(item.id)}>
        <div className="notification-item">
            <i className={item.icon}></i>
            <span>{item.content}</span>
        </div>
    </Link>
)
