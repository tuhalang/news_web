import React, {useState} from "react"
import {Drawer, Menu} from "antd";
import {isMobile} from "react-device-detect";

import './index.scss'
import _ from "lodash"
import Header from "./header/Header";
import {useDispatch, useSelector} from "react-redux";
import {showDrawer} from "../../redux/actions";
import {LeftOutlined} from "@ant-design/icons";
import {PRIMARY_COLOR} from "../../utils/constant";
import Subscriber from "../modal/Subscriber";
import OTPModal from "../modal/Otp";
import Video from "../video/Video";
import {paramsUrl} from "../_Function";
import {useHistory} from "react-router";

const {SubMenu} = Menu;

const Layout = ({children}) => {

    const dispatch = useDispatch()
    const history = useHistory()
    const {drawer, otp} = useSelector(state => state.common)
    const {categories} = useSelector(state => state.common)

    function renderCategories(category) {
        let result = []
        // result.push(category)
        if (category.subCategories && category.subCategories.length > 0) {
            // for (let sub of category.subCategories) {
            //     result = [...result, ...renderCategories(sub)]
            // }
            result.push(<SubMenu key={category.id} title={category.name}>
                {category.subCategories.map(sub => renderCategories(sub))}
            </SubMenu>)
        } else {
            result.push(<Menu.Item key={category.id}>{category.name}</Menu.Item>)
        }
        return result
    }

    return (
        <div className={`layout flex flex-column absolute items-center`} style={isMobile ? {
            width: '100%'
        } : {}}>
            {/*<Menu onChangeShow={onChangeShow} show={show} />*/}
            <Drawer
                title={<span className="title">Sport news</span>}
                placement="left"
                onClose={() => dispatch(showDrawer())}
                closeIcon={<LeftOutlined style={{color: "white"}}/>}
                visible={drawer}
                headerStyle={{
                    backgroundColor: PRIMARY_COLOR,
                }}
                key="drawer"
                className="drawer"
            >
                <Menu onSelect={({key, selectedKeys}) => {
                    paramsUrl.set({category: selectedKeys[0]})
                    history.go(0)
                }}
                      mode="inline"
                >
                    {
                        categories && categories.map(category => {
                            if(category.parentId === null)
                                return renderCategories(category)
                        })
                    }
                </Menu>
            </Drawer>

            <div className="header justify-between items-center">
                <Header/>
            </div>
            <div className="content w-full container">{children}</div>
            <OTPModal visible={otp}/>
            <Video/>
        </div>
    )
}

export default Layout
